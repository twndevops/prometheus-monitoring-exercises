#### Notes for "Module 16 - Monitoring with Prometheus" exercises

EXERCISE 1: Create a K8s cluster running a Java-MySQL app, using Ingress to enable browser access.

- Used the Java-Gradle project [`bootcamp-java-mysql`](https://gitlab.com/devops-bootcamp3/bootcamp-java-mysql/-/tree/feature/monitoring), `feature/monitoring` to build the Java app deployed into the cluster.

  - Built app image for `bootcamp-java-mysql` for multiple Linux architectures:

    ```
    docker buildx build --push --platform linux/arm64/v8,linux/amd64 --tag upnata/bootcamp-docker-java-mysql-project:1.0-monitoring .
    ```

  - Env vars to set to connect the app to the DB can be found in `bootcamp-java-mysql/src/main/java/com/example/DatabaseConfig.java`.

- Modified / Used the K8s component configs and Ansible playbook configured in [`ansible-exercises`](https://gitlab.com/twndevops/ansible-exercises) to create an EKS cluster comprising 2 nodes and deploying 2 MySQL replicas, 3 Java app replicas, a Secret and ConfigMap to enable app-DB connectivity, and Nginx-Ingress Controller with an Ingress rule to allow browser access to the app.

- Regarding the playbook:

  - No `ansible.cfg` is required to execute it, since all plays run on `localhost`.

  - After executing, wait a few minutes. It takes a little time before the Java app loads in the browser.

EXERCISE 2: Start collecting metrics for the app and 3rd-party services (MySQL, Nginx-Ingress).

- Added a new play `Install Prometheus Monitoring Stack (Prom MS) in a new "monitoring" namespace` to automate:

  - creating a new namespace
  - installing [`kube-prometheus-stack` Helm chart](https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack) to deploy Prometheus operator and monitoring stack K8s components into the new namespace. To see the deployed components (managed by Prom operator), run `kubectl get all -n monitoring`.

- Both MySQL and Nginx-Ingress Controller Helm charts support metrics scraping with Prometheus. There's no need to manually deploy separate exporters.

- [Configured metrics scraping](https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx#prometheus-metrics) for Nginx-Ingress Controller in `ingress-values.yaml`.

- [Configured metrics scraping](https://artifacthub.io/packages/helm/bitnami/mysql#metrics-parameters) for MySQL in `mysql-values.yaml`.

- Overview of the Java-Gradle app metrics scraping configuration:

  - The app runs on port 8080 and exposes metrics on port 8081 (NOT `/metrics` endpoint).

    - To test the Java app client is exposing metrics, add the following `path` configuration to the inline Ingress rule in `8-k8s-cluster-mysql-helm.yaml`:

      ```
      - path: /metrics
        pathType: Prefix
        backend:
          service:
            name: java-gradle-app-svc
            port:
              number: 8081
      ```

      Execute the playbook and load `[LoadBalancer_hostname]/metrics` in the browser. You should see the configured Java metrics. Remove the `path` configuration after testing to avoid exposing metrics on the frontend.

  - In `java-gradle-app.yaml`:

    - Exposed both ports on the app containers.

    - Configured the app Service to access and expose both container ports: 8080 publicly / externally and 8081 to a `ServiceMonitor`.

    - Configured a `ServiceMonitor` CRD to tell Prometheus to add a new job scraping the Java-Gradle app target. All set `spec` fields are required for Prometheus to pick up the new target. After creating it, wait a few minutes. It takes a little time before the ServiceMonitor appears in Prom UI, Status > Targets.

      - Prometheus uses the `ServiceMonitor` label `release: monitoring` to discover `ServiceMonitor`s in any cluster namespace.

        - `monitoring` is the Prometheus MS Helm release name.

      - Since the ServiceMonitor is deployed in the same NS as the Java-Gradle app (default) but in a different NS than Prometheus (monitoring), must explicitly set `spec.namespaceSelector` to indicate the new scrapable endpoint is in the default NS.

        - Endpoint = Service.

      - A Service's `spec.selector` label(s) are matched to `metadata.labels` (OR `spec.selector.matchLabels`) on a Deployment resource. Likewise, a ServiceMonitor's `spec.selector` label(s) are matched to `metadata.labels` on a Service resource. The Service resource MUST have the expected label(s) for the ServiceMonitor to discover it.

    - `spec.endpoints[].port` is the NAME of the Service port used to access the container port 8081 exposing app metrics.

      - Since `spec.endpoints[].path` is empty, Prom will scrape `/metrics` path.

- Port forward to view Prom UI: `kubectl port-forward service/monitoring-kube-prometheus-prometheus :9090 -n monitoring &`
  - In Status > Targets, there should be 3 new `serviceMonitor`s / scrape jobs collecting metrics.
  - In Graph, type:
    - `nginx` to see available ingress metrics
    - `mysql` to see available DB metrics
    - `java_app_http_requests_total` (Counter), `java_app_inprogress_requests` (Gauge), and `java_app_http_requests_created` (Gauge) for custom-configured Java app metrics.

EXERCISE 3: Configure alert rules for critical cluster issues.

- The last Ansible playbook play `Deploy the alert rules, Alertmanager secret, and Alertmanager configs` automates deploying all the configured alert rules.

- All `PrometheusRule`s must:

  - be created in the same namespace as Prom Monitoring Stack.
  - have 2 labels to be discovered by Prometheus:
    - `app: kube-prometheus-stack`, where the value is the name of the Prom Monitoring Stack Helm chart
    - `release: monitoring`, where the value is the name of the Monitoring Stack's Helm release

1. Nginx-Ingress Controller alert condition: "> 5% of HTTP requests get a status 4xx"

```
(sum(rate(nginx_ingress_controller_requests{status=~"4.."}[5m])) / sum(rate(nginx_ingress_controller_requests[5m]))) * 100 > 5
```

Expression breakdown:

- `nginx_ingress_controller_requests{status=~"4.."}` filters for only 4xx requests.
- `rate` = per second rate over 5 mins = 300 rate values
- `sum` = total/ aggregated rate of 300 rate values
- The ratio of 4xx over all requests times 100 yields a percentage.

2. MySQL alert condition: "All MySQL instances are down"

```
sum(mysql_up) == 0
```

Expression breakdown:

- `mysql_up` is a boolean metric (0 = pod is down, 1 = pod is up).
- In a healthy cluster, `sum(mysql_up)` will equal the number of MySQL replicas. If ALL are down, the alert is triggered.
- ALTERNATIVE expression to trigger the alert if 1+ replicas are down: `avg(mysql_up) < 1`. If all replicas are up, the avg will be 1.

3. MySQL alert condition: "MySQL has too many connections"

```
count(mysql_global_status_threads_connected > mysql_global_variables_max_connections) > 0
```

Expression breakdown:

- `mysql_global_status_threads_connected > mysql_global_variables_max_connections` will return 0, 1, or 2 MySQL instance results.
- `count` will aggregate the returned results to a number (b/w 0 and 2 replicas).
- If > 0 instances have more connected threads currently than the max allowed, the alert is triggered.
- In a healthy cluster, `count(mysql_global_status_threads_connected < mysql_global_variables_max_connections)` (i.e. the opposite check) evaluates to 2.

- [Reference for MySQL metrics](https://www.alibabacloud.com/blog/observability-%7C-how-to-use-prometheus-to-monitor-mysql_600363)
  - NOTE: metrics used together in an expression must be comparable in Prometheus (i.e. have the same labels)!

4. Java-Gradle app alert condition: "Receiving too many incoming requests"

- Did NOT use an available custom metric `java_app_http_requests_created` (Gauge) b/c the values scraped by Prometheus were absurd (i.e. 1.70829911632E9).

```
count(rate(java_app_http_requests_total[5m]) * 100 > 90) > 0
```

Expression breakdown:

- `java_app_http_requests_total` (Counter) = Total Requests.
- `rate` = per second rate over 5 mins = 300 rate values. If the scaled rate exceeds 90% on 1+ app instances, they will be returned as results.
- `count` will aggregate the returned results to a number (b/w 0 and 3 replicas).
- If 1+ app instances are nearing traffic overload, the alert is triggered.

5. Java-Gradle app alert condition: "Receiving too many concurrent requests"

```
count(rate(java_app_inprogress_requests[5m]) * 100 > 90) > 0
```

Expression breakdown:

- `java_app_inprogress_requests` (Gauge) = Inprogress Requests.
- Same notes as previous expression.

6. StatefulSets alert condition: Replicas mismatch (i.e. notify if a replica is down)

- In default namespace, there are 2 sts's from the MySQL Helm chart install:
  - `java-gradle-app-db-mysql-primary`
  - `java-gradle-app-db-mysql-secondary`
- In monitoring namespace, there are 2 sts's from the Prom MS Helm chart install:
  - `alertmanager-monitoring-kube-prometheus-alertmanager`
  - `prometheus-monitoring-kube-prometheus-prometheus`.

Available Statefulset metrics:

- `kube_statefulset_replicas` = sts desired replica count
- `kube_statefulset_status_replicas` = count of replicas currently considered part of the sts
- `kube_statefulset_status_replicas_ready` = sts ready replica count
  - ALTERNATIVE: `kube_statefulset_status_replicas_available`

```
sum(kube_statefulset_status_replicas_available{namespace="default"}) < sum(kube_statefulset_replicas{namespace="default"})
```

Expression breakdown:

- This expression checks only MySQL sts replicas in the default namespace.
- If the available / ready replica count is less than desired, the alert is triggered.
- An ALTERNATIVE expression that uses `statefulset` label to filter:: `sum(kube_statefulset_status_replicas_available{statefulset=~".*-mysql-.*"}) < sum(kube_statefulset_replicas{statefulset=~".*-mysql-.*"})`.

  - The first `.*` is the Helm release name (determined by Ansible playbook user).
  - The second `.*` is either `primary` or `secondary`.

EXERCISE 4: Send all issues related to the Java app or MySQL to a developer team's Slack channel. Send all issues related to the Nginx-Ingress Controller or K8s components to a K8s administrator's email address.

- The last Ansible playbook play `Deploy the alert rules, Alertmanager secret, and Alertmanager configs` automates deploying the Alertmanager secret and then the 2 configured Alertmanagers.

- [Reference for AlertmanagerConfig](https://docs.openshift.com/container-platform/4.14/rest_api/monitoring_apis/alertmanagerconfig-monitoring-coreos-com-v1beta1.html) (a CRD that is an extension of K8s monitoring API)

- NOTES on `alertmanager-secret.yaml`:

  - The secret must be deployed to the same namespace as the `AlertmanagerConfig`s and be accessible by the Prometheus operator.

  - ISSUE: base64-encoding the `data` caused an error when creating the secret: `Error from server (BadRequest): error when creating "alertmanager-secret.yaml": Secret in version "v1" cannot be handled as a Secret: illegal base64 data at input byte 5`.

    - WORKAROUND: Used `stringData` instead of `data` to pass non-encoded secret values.

  - Since the `upnata@gmail.com` email has 2FA configured, created a [Google App Password](https://myaccount.google.com/apppasswords) to allow temporary access to the Gmail account so AlertManager would be able to email alerts to the other `upnata.apple@gmail.com` account. The provided App Password has spaces, which must be removed for the password to work. (The app password has since been deleted.)

  - Created a new private Slack channel in `CS Prep 73A` workspace called `prom-alerts`. Under Apps > Add Apps > searched "Incoming WebHooks" and added the integration to the `prom-alerts` channel. Stored the WebHook URL in the secret for AlertManager to send notifications to the Slack channel. (The Incoming WebHooks integration has since been removed.)

- Port forward to view AlertManager UI: `kubectl port-forward service/monitoring-kube-prometheus-alertmanager :9093 -n monitoring &`. Click Status > Final Alertmanager configuration after merging `alertmanager-email.yaml` and `alertmanager-slack` w/ default configuration:

```
global:
  resolve_timeout: 5m
  http_config:
    follow_redirects: true
    enable_http2: true
  smtp_hello: localhost
  smtp_require_tls: true
  pagerduty_url: https://events.pagerduty.com/v2/enqueue
  opsgenie_api_url: https://api.opsgenie.com/
  wechat_api_url: https://qyapi.weixin.qq.com/cgi-bin/
  victorops_api_url: https://alert.victorops.com/integrations/generic/20131114/alert/
  telegram_api_url: https://api.telegram.org
  webex_api_url: https://webexapis.com/v1/messages
route:
  receiver: "null"
  group_by:
  - namespace
  continue: false
  routes:
  - receiver: monitoring/email-alert-config/email
    matchers:
    - namespace="monitoring"
    continue: true
    routes:
    - receiver: monitoring/email-alert-config/email
      matchers:
      - alertname="HighPercent4xxRequests"
      - alertname="MySQLStatefulSetReplicasMismatch"
      continue: false
    group_interval: 1m
    repeat_interval: 1m
  - receiver: monitoring/slack-alert-config/slack
    matchers:
    - namespace="monitoring"
    continue: true
    routes:
    - receiver: monitoring/slack-alert-config/slack
      matchers:
      - alertname="AllDBPodsDown"
      - alertname="HighPercent4xxRequests"
      - alertname="TooManyConcurrentRequests"
      - alertname="TooManyDBConnections"
      - alertname="TooManyIncomingRequests"
      continue: false
    group_interval: 1m
    repeat_interval: 1m
  - receiver: "null"
    matchers:
    - alertname="Watchdog"
    continue: false
  group_wait: 30s
  group_interval: 5m
  repeat_interval: 12h
inhibit_rules:
- source_matchers:
  - severity="critical"
  target_matchers:
  - severity=~"warning|info"
  equal:
  - namespace
  - alertname
- source_matchers:
  - severity="warning"
  target_matchers:
  - severity="info"
  equal:
  - namespace
  - alertname
- source_matchers:
  - alertname="InfoInhibitor"
  target_matchers:
  - severity="info"
  equal:
  - namespace
- target_matchers:
  - alertname="InfoInhibitor"
receivers:
- name: "null"
- name: monitoring/email-alert-config/email
  email_configs:
  - send_resolved: true
    to: upnata.apple@gmail.com
    from: upnata@gmail.com
    hello: localhost
    smarthost: smtp.gmail.com:587
    auth_username: upnata@gmail.com
    auth_password: <secret>
    auth_identity: upnata@gmail.com
    headers:
      From: upnata@gmail.com
      Subject: '{{ template "email.default.subject" . }}'
      To: upnata.apple@gmail.com
    html: '{{ template "email.default.html" . }}'
    require_tls: true
- name: monitoring/slack-alert-config/slack
  slack_configs:
  - send_resolved: true
    http_config:
      follow_redirects: true
      enable_http2: true
    api_url: <secret>
    channel: '#prom-alerts'
    username: '{{ template "slack.default.username" . }}'
    color: '{{ if eq .Status "firing" }}danger{{ else }}good{{ end }}'
    title: '{{ template "slack.default.title" . }}'
    title_link: '{{ template "slack.default.titlelink" . }}'
    pretext: '{{ template "slack.default.pretext" . }}'
    text: '{{ template "slack.default.text" . }}'
    short_fields: false
    footer: '{{ template "slack.default.footer" . }}'
    fallback: '{{ template "slack.default.fallback" . }}'
    callback_id: '{{ template "slack.default.callbackid" . }}'
    icon_emoji: '{{ template "slack.default.iconemoji" . }}'
    icon_url: '{{ template "slack.default.iconurl" . }}'
    link_names: false
templates:
- /etc/alertmanager/config/*.tmpl
```

- All `PrometheusRule`s have a label `namespace: monitoring`. If this label is not present on the alert rule, no notification will go out via the configured channel.

  - In the merged Alertmanager configuration above:
    - `route.routes[0].matchers` and `route.routes[1].matchers` both contain 1 list item: `namespace="monitoring"`.
      - This label is automatically applied on merge. The label CANNOT be removed, and its value CANNOT be changed.
    - `route.routes[0].continue` and `route.routes[1].continue` are both `true`.
    - `route.routes[0].receiver: monitoring/*-alert-config/*` means that Alertmanager will first match `namespace="monitoring"` on the alert rule, then continue down the tree to the child routes (`route.routes.routes`) and match ANY (not ALL) of the child route labels to determine the notification channel (`email` or `slack`).

- NOTES on `AlertmanagerConfig`:

  - `spec.route.receiver` MUST be set for the `AlertmanagerConfig` to merge with the default config!

    - This is the fallback receiver when no child routes are matched by AlertManager to trigger an alert. It must be one of the receivers defined in `spec.receivers`.
    - When not set, the AlertManager UI shows the error [PrometheusOperatorRejectedResources](https://runbooks.prometheus-operator.dev/runbooks/prometheus-operator/prometheusoperatorrejectedresources/). The Prometheus operator pod logs the error: `msg="skipping alertmanagerconfig" error="root route must define a receiver"`.

  - Moving `spec.route.routes[0].matchers` to `spec.route.matchers` fails. No notifications are sent.

  - `spec.route.groupInterval` overrides `spec.route.repeatInterval` if the latter is a shorter time period. `repeatInterval` is how often to send a notification as long as the issue is unresolved.

- FOLLOWUP OPTIMIZATIONS:
  - Alert rule `description`s' `{{ $labels }}` don't interpolate in the email notifications (but `{{ $value }}` does). Neither `summary` nor `description` is sent in the Slack notifications.
  - Consolidate 2 `AlertmanagerConfig`s into 1.
    - ISSUE ENCOUNTERED using 1 config: The label `namespace: monitoring` applied at the root route level matched ALL of the alert rules and directed them all to the root route receiver, so EITHER `email` or `slack`.

EXERCISE 5: Trigger email and Slack alert notifications

- In `alertmanager-slack.yaml`, uncomment the `HighPercent4xxRequests` label matcher.
- Load the URL output by the Ansible playbook w/ a non-existent path: `[Loadbalancer_hostname]/metrics`. Keep refreshing the page to trigger non-stop HTTP requests. In Prom UI > Alerts, you should see the `HighPercent4xxRequests` alert go from Inactive -> Pending -> Firing.
- The emails and Slack notifications should be sent promptly.
